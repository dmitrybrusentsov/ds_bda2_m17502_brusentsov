package com.mycompany.testlab2;

import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.AnalysisException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

/**
 * Приложение, которое вычисляет среднюю зарплату сотрудников предприятия за
 * один календарный год по 3 возрастным категориям. 1 возрастная категория -
 * сотрудники чей возраст от 18 до 30 2 возрастная категория - сотрудники чей
 * возраст от 31 до 50 3 возрастная категория - сотрудники чей возраст от 51 до
 * 65
 *
 * @author Dmitry
 * @version 1.0
 */
public class SparkTest {

    /**
     * args[0] - путь до файла с данными для базы
     *
     * @param args
     * @throws AnalysisException
     */
    public static void main(String[] args) throws AnalysisException {
        try {//Создаем spark сессию
            SparkSession spark = SparkSession
                    .builder()
                    .appName("Java Spark SQL basic example")
                    .config("spark.master", "local")
                    .getOrCreate();
            //Путь до файла с базой данных
            String path = "";
            //Проверка входного аргумента на null
            if (args.length != 0 && args[0] != null && !args[0].isEmpty()) {
                path = args[0];
            }
            //Запуск spark-задачи
            run(spark, path);
            //Завершение работы spark
            spark.stop();
        } catch (Exception e) {
            System.out.println("Error! " + "Ошибка запуска задачи");
        }
    }

    /**
     * Метод запуска spark
     *
     * @param spark - сессия
     * @param path - путь до файла с базой данных
     * @throws AnalysisException
     */
    private static void run(SparkSession spark, String path) throws AnalysisException {
        //Создаем Dataset из данных входного файла формата json
        Dataset<Row> df = spark.read().json(path);
        //Отображаем содержимое таблицы данных
        df.show();
        //Подсчет людей по возрасту
        df.groupBy("age").count().show();
        //Создаем таблицу "people" sql
        df.createOrReplaceTempView("people");
        //Получаем данные по 1 возрастной категории от 18 до 30 лет
        Dataset<Row> group1 = spark.sql("SELECT name FROM people WHERE age BETWEEN 18 AND 30");
        //Отображаем содержимое
        group1.show();
        //Получаем данные по 2 возрастной категории от 31 до 50 лет
        Dataset<Row> group2 = spark.sql("SELECT name FROM people WHERE age BETWEEN 31 AND 50");
        //Отображаем содержимое
        group2.show();
        //Получаем данные по 3 возрастной категории от 51 до 65 лет
        Dataset<Row> group3 = spark.sql("SELECT name FROM people WHERE age BETWEEN 51 AND 65");
        //Отображаем содержимое
        group3.show();
        //Получаем среднюю зарплату по 1 возрастной категории от 18 до 30 лет
        Dataset<Row> average_salary1 = spark.sql("SELECT AVG(salary) AS Average_salary FROM people WHERE age BETWEEN 18 AND 30");
        //Отображаем содержимое
        average_salary1.show();
        //Получаем среднюю зарплату по 2 возрастной категории от 31 до 50 лет
        Dataset<Row> average_salary2 = spark.sql("SELECT AVG(salary) AS Average_salary FROM people WHERE age BETWEEN 31 AND 50");
        //Отображаем содержимое
        average_salary2.show();
        //Получаем среднюю зарплату по 3 возрастной категории от 51 до 65 лет
        Dataset<Row> average_salary3 = spark.sql("SELECT AVG(salary) AS Average_salary FROM people WHERE age BETWEEN 51 AND 65");
        //Отображаем содержимое
        average_salary3.show();
        //Получаем значение средней зарплаты по 1 возрастной категории от 18 до 30 лет
        String as_group1 = average_salary1.first().get(0).toString();
        System.out.println("average salary_group1 = " + as_group1);
        //Получаем значение средней зарплаты по 2 возрастной категории от 31 до 50 лет
        String as_group2 = average_salary2.first().get(0).toString();
        System.out.println("average salary_group2 = " + as_group2);
        //Получаем значение средней зарплаты по 23 возрастной категории от 51 до 65 лет
        String as_group3 = average_salary3.first().get(0).toString();
        System.out.println("average salary_group3 = " + as_group3);
        //Создаем массив данных для записи в результирующий файл
        String data[] = new String[3];
        data[0] = "age category 18-30" + ", " + as_group1;
        data[1] = "age category 31-50" + ", " + as_group2;
        data[2] = "age category 51-65" + ", " + as_group3;
        try {
            //Запись данных в выходной файл
            createOutputFile(data);
        } catch (IOException ex) {
            System.out.println("Error write output file");
        }
    }
    
    /**
     * Метод записи текстового файла
     *
     * @param data - массив данных со среднеми зарплатами по 3 возрастным
     * категориям
     * @throws IOException
     */
    private static void createOutputFile(String[] data) throws IOException {
        String filePath = "resultLab2.txt";
        //Создание потока записи в текстовый файл
        try (FileWriter wr = new FileWriter(filePath, false)) {
            for (int i = 0; i < data.length; i++) {
                //Добавление данных в файл
                wr.write(data[i]);
                wr.write("\n");
            }
            wr.flush();
        } catch (IOException ex) {
            System.out.println("Ошибка записи файла");
        }
    }
}
