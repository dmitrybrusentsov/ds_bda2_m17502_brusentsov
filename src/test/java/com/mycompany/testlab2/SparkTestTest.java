package com.mycompany.testlab2;

import org.apache.spark.sql.*;
import java.util.*;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Тестовый класс
 * @author Dmitry
 * @version 1.0
 */
public class SparkTestTest {

    @Test
    public void test_select_bd() {
        //Создаем spark сессию
        SparkSession spark = SparkSession
                .builder()
                .appName("Java Spark SQL basic example")
                .config("spark.master", "local")
                .getOrCreate();
        //Создаем лист данных с 2 сотрудниками
        List<String> data = Arrays.asList("[{\"name\":\"Ivanov Ivan Ivanovich\",\"passport number\":\"536824\",\"passport serial number\":\"2553\",\"age\":\"54\",\"month\":\"January\",\"salary\":\"243545\"},"
                + "{\"name\":\"Petrov Petr Petrovich\",\"passport number\":\"536824\",\"passport serial number\":\"2553\",\"age\":\"54\",\"month\":\"January\",\"salary\":\"243545\"}]");
        Dataset<Row> df = spark.createDataset(data, Encoders.STRING()).toDF();
        //Создаем таблицу "people" sql
        df.createOrReplaceTempView("people");
        //Получаем данные по сотрудникам чей возраст от 50 до 60 лет
        Dataset<Row> group = spark.sql("SELECT name FROM people WHERE age BETWEEN 50 AND 60");
        //Сравниваем количество сотрудников
        assertEquals(2, group.count());
        //Завершение работы spark
        spark.stop();
    }
}
